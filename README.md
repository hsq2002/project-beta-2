# CarCar

Team:

* Person 1 - Which microservice? Humza - Services
* Luis - Sales microservice

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

My service models consisted of the technician, appointments, and the shared AutomobileVO (value obeject). Our Automobile value object consists of a vin property. This property is utilized as the optimal way to identify a specific car. The car is shared between our inventory and appointments, requiring a VO + poller, allowing the object to exist and operate in both entities, synchronously.

The role of my VIN was to operate in the service history, allowing the VIN of my appointments to detect any similar sequence with that of the automobile VIN.








## Sales microservice

Explain your models and integration with the inventory
microservice, here.

In our system, we utilized different models that worked together with the inventory microservice. Each model has its own approach to connecting with the inventory microservice. One of these models is called AutomobileVO, and it focusus on a sinle peice of information known as the VIN(Vehicle Identification Number). This vin servers as a unique identifier for each car in our inventory, allowing us to easily differentiate and identify individual vehicles. The SalesPerson model has three properties, the salesperson's first and last name, and their employee number, which is used to identify them uniquely.

The Customer model has 4 properties: the customer's first and last name, address, and phone number. 

Finally, the Sales model has 4 properties: VIN, salesperson, customer, and price(sales price). VIN, salesperson, and customer are properties connected to the previous models through foreign keys. We've also included methods that generates restful URL's. The URL is obtained by using the ID value to retrieve specific sale objects and then returning the corresponding URL. To connect with inventory microservice, we used a polling method. This means that we periodically checked for updates in the AutomobileVO model. By doing that, we were able to get the VIN numver along with the information from other models mentioned ealier. This allowed us to crease a new sale and record that sale in a seperate URL page all have the details of the sale organized by the Salesperson, customer of who purchased the vehicle, the VIN, and price.