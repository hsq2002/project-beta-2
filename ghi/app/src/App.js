import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import VehicleModelForm from "./Inventory/VehicleModelForm";
import VehicleModelList from "./Inventory/VehicleModelList";
import ManufacturerForm from "./Inventory/ManufacturerForm";
import ManufacturerList from "./Inventory/ManufacturerList"
import AutomobileForm from "./Inventory/AutomobileForm";
import AutomobileList from "./Inventory/AutomobileList";
import TechnicianForm from "./Services/TechnicianForm";
import TechnicianList from "./Services/TechnicianList";
import AppointmentForm from "./Services/AppointmentForm";
import AppointmentList from "./Services/AppointmentList";
import ServiceHistory from "./Services/ServiceHistory";


import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import Salespeoplelist from './SalespersonList';
import SalespersonForm from './Sales/SalespersonForm';
import SaleForm from './Sales/SalesForm'
import SaleList from './Sales/SalesList'
import SalespersonHistory from './SalespersonHistory';
function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/VehicleForm" element={<VehicleModelForm />} />
          <Route path="/VehicleList" element={<VehicleModelList />} />
          <Route path="/ManufacturerForm" element={<ManufacturerForm />} />
          <Route path="/ManufacturerList" element={<ManufacturerList />} />
          <Route path="/AutomobileForm" element={<AutomobileForm />} />
          <Route path="/AutomobileList" element={<AutomobileList />} />

          <Route path="/" element={<MainPage />} />

          <Route path="/TechnicianForm" element={<TechnicianForm />} />
          <Route path="/TechnicianList" element={<TechnicianList />} />
          <Route path="/AppointmentForm" element={<AppointmentForm />} />
          <Route path="/AppointmentList" element={<AppointmentList />} />
          <Route path="/ServiceHistory" element={<ServiceHistory />} />
          {/* <Route path='/customers'> */}
          <Route path="/customers" element={<CustomerList />} />
          <Route path="/customers/new" element={<CustomerForm />} />
          {/* </Route>
          <Route path='/salespeople'> */}
          <Route path="/salespeople" element={<Salespeoplelist />} />
          
          <Route path="/salespeople/new" element={<SalespersonForm />} />
          {/* </Route> */}
          {/* <Route path='/sales'> */}
            <Route path="/saleslist" element={<SaleList />} />
            <Route path="/sales/new" element={<SaleForm />}></Route>
            <Route path="/history" element={<SalespersonHistory />} />
          {/* </Route> */}

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
