import React, {useState } from "react";

function CustomerForm() {
    const [first, setFirst] = useState('');
    const [last, setLast] = useState('');
    const [address, setAddress] = useState('');
    const [phonenumber, setPhoneNumber] = useState('');

    const handleFirstChange = (event) => {
        const value = event.target.value;
        setFirst(value);
    };
    const handleLastChange = (event) => {
        const value = event.target.value;
        setLast(value);
    };
    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value);
    };
    const handlePhoneNumberChange = (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    };
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.first_name = first;
        data.last_name = last;
        data.address = address;
        data.phone_number = phonenumber;

        const customerUrl = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            },
        };
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            setFirst('');
            setLast('');
            setAddress('');
            setPhoneNumber('');
        }
    };
    
    return (
      
        <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Customer </h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input value={first} onChange={handleFirstChange} placeholder="first name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={last} onChange={handleLastChange} placeholder="last name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="last_name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={address} onChange={handleAddressChange} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
              <label htmlFor="address">Address</label>
            </div>
            <div className="form-floating mb-3">
              <input value={phonenumber} onChange={handlePhoneNumberChange} placeholder="Phone number" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="phone_number">Phone Number</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    )
}
export default CustomerForm
