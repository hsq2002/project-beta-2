import React, {useEffect, useState} from "react";

function AutomobileForm() {
    const [models, setModels] = useState([]);
    const [color, setColor] = useState("");
    const [year, setYear] = useState("");
    const [vin, setVIN] = useState("");
    const [model, setModel] = useState("");

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handleYearChange = (event) => {
        const value = event.target.value;
        setYear(value);
    }

    const handleVINChange = (event) => {
        const value = event.target.value;
        setVIN(value);
    }

    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
    }



    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setModels(data.models)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;


        const Automobileurl = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(Automobileurl, fetchConfig);
        if (response.ok) {
            const newAutomobile = await response.json();

            setColor("");
            setYear("");
            setVIN("");
            setModel("");


        }
    }

    useEffect(() => {
        fetchData();
        }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create an Automobile</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input value={color} onChange={handleColorChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={year} onChange={handleYearChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input value={vin} onChange={handleVINChange} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="picture">VIN</label>
              </div>
              <div className="mb-3">
                <select value={model} onChange={handleModelChange} required id="manufacturer_id" name="manufacturer_id"  className="form-select">
                  <option value="">Choose a model</option>
                  {models.map(model => {
                    return(
                        <option key={model.id} value={model.id}>
                        {model.name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}


export default AutomobileForm;
