import {useState, useEffect} from "react";

function AutomobileList() {
    const [automobiles, setAutomobile] = useState([])
    const LoadAutomobiles = async () => {

        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setAutomobile(data.autos);
        }
    }

    useEffect(() => {
        LoadAutomobiles()
    },[])

    const handleDelete = async (event) => {
      const url = `http://localhost:8100/api/automobiles/${event.target.id}`;

      const fetchConfigs = {
        method: "Delete",
        headers: {
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(url, fetchConfigs);
      const data = await response.json();

      setAutomobile(
        automobiles.filter(
          (automobile) => String(automobile.id) !== event.target.id
        )
      );
    };

    return (
        <>
        <div className="container">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Color</th>
                <th>Year</th>
                <th>vin</th>
                <th>model_id</th>
              </tr>
            </thead>
            <tbody>
              {automobiles?.map(automobile => {
                return (
                  <tr key={automobile.id}>
                    <td>{ automobile.color }</td>
                    <td>{ automobile.year }</td>
                    <td>{ automobile.vin }</td>
                    <td>{ automobile.id }</td>
                    <td>
                      <button
                        onClick={handleDelete}
                        vin={automobile.vin}
                        className="btn btn-danger">Delete
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        </>
    )
}

export default AutomobileList
