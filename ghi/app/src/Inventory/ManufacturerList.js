import React, {useState, useEffect} from "react";
function ManufacturerList() {
    const [manufacturers, setManufacturer] = useState([]);
    const LoadManufacturer = async () => {

        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setManufacturer(data.manufacturers);
        }
    }

    useEffect(() => {
        LoadManufacturer()
    },[])

    const handleDelete = async (event) => {
      const url = `http://localhost:8100/api/manufacturers/${event.target.id}`;

      const fetchConfigs = {
        method: "Delete",
        headers: {
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(url, fetchConfigs);
      const data = await response.json();

      setManufacturer(
        manufacturers.filter(
          (manufacturer) => String(manufacturer.id) !== event.target.id
        )
      );
    };

    return (
        <>
        <div className="container">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
              </tr>
            </thead>
            <tbody>

              {manufacturers?.map(manufacturer => {
                return (
                  <tr key={manufacturer.id}>
                    <td>{ manufacturer.name }</td>
                    <td>
                      <button
                        onClick={handleDelete}
                        id={manufacturer.id}
                        className="btn btn-danger">Delete
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        </>
      );
    }

export default ManufacturerList;
