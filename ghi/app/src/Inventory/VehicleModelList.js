import {useState, useEffect} from "react";

function VehicleModelList() {
    const [vehicles, setVehicle] = useState([])
    const LoadVehicles = async () => {

        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setVehicle(data.models);
        }
    }

    useEffect(() => {
        LoadVehicles()
    },[])

    const handleDelete = async (event) => {
      const url = `http://localhost:8100/api/models/${event.target.id}`;

      const fetchConfigs = {
        method: "Delete",
        headers: {
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(url, fetchConfigs);
      const data = await response.json();

      setVehicle(
        vehicles.filter(
          (vehicle) => String(vehicle.id) !== event.target.id
        )
      );
    };

    return (
        <>
        <div className="container">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Picture</th>
                <th>Manufacturer</th>
                <th>Manufacturer ID</th>
              </tr>
            </thead>
            <tbody>
              {vehicles?.map(vehicle => {
                return (
                  <tr key={vehicle.id}>
                    <td>{ vehicle.name }</td>
                    <td>
                        <img src={ vehicle.picture_url } width="300" height="200"></img>
                    </td>
                    <td>{ vehicle.manufacturer.name }</td>
                    <td>{ vehicle.manufacturer.id }</td>
                    <td>
                    <button
                        onClick={handleDelete}
                        id={vehicle.id}
                        className="btn btn-danger">Delete
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        </>
    )
}

export default VehicleModelList
