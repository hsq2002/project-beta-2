import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="inventoryDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="inventoryDropdown">
                <li><NavLink className="dropdown-item" to="/VehicleForm">Create Vehicle Model</NavLink></li>
                <li><NavLink className="dropdown-item" to="/VehicleList">List of Vehicles</NavLink></li>
                <li><NavLink className="dropdown-item" to="/ManufacturerForm">Create Manufacturer</NavLink></li>
                <li><NavLink className="dropdown-item" to="/ManufacturerList">List of Manufacturers</NavLink></li>
                <li><NavLink className="dropdown-item" to="/AutomobileForm">Create Automobile</NavLink></li>
                <li><NavLink className="dropdown-item" to="/AutomobileList">List of Automobiles</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="servicesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Services
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="servicesDropdown">
                <NavLink className="dropdown-item" to="/TechnicianForm">Create Technician</NavLink>
                <NavLink className="dropdown-item" to="/TechnicianList">List of Technicians</NavLink>
                <NavLink className="dropdown-item" to="/AppointmentForm">Create Appointment</NavLink>
                <NavLink className="dropdown-item" to="/AppointmentList">List of Appointments</NavLink>
                <NavLink className="dropdown-item" to="/ServiceHistory">Service History</NavLink>
                </ul>
          </li>
          <li className='nav-item' aria-labelledby="SalesDropDown">
            <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople/new">Add a Salesperson</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople">List of salespeople</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customers">Customers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customers/new">Add a Customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/saleslist">Sales</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales/new">New Sale</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/history">Sales Person History</NavLink>
            </li>            
            </li>
        </ul>
      </div>
    </div>
  </nav>
);
}

export default Nav;
