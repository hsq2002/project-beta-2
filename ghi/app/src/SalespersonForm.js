import React, { useState } from 'react';

function SalespersonForm() {
    const [first, setFirst] = useState(' ');
    const [last, setLast] = useState(' ');
    const [employeeId, setEmployeeId] = useState(' ')


    const handleFirstChange = (event) => {
        const value = event.target.value;
        setFirst(value)
    }
    const handleLastChange = (event) => {
        const value = event.target.value;
        setLast(value)
    }
    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value)
    }
    const handleSubmit= async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = first;
        data.last_name = last;
        data.employee_id = employeeId;

        const url = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newSalesPerson = await response.json();
            console.log(newSalesPerson);
            setFirst('');
            setLast('');
            setEmployeeId('');
        };
    };
    return (
        <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Sales person </h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input value={first} onChange={handleFirstChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={last} onChange={handleLastChange} placeholder="Last" required type="text" name="last" id="last" className="form-control" />
              <label htmlFor="">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={employeeId} onChange={handleEmployeeIdChange} placeholder="Last" required type="text" name="last" id="last" className="form-control" />
              <label htmlFor="">Employee Id</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
    )
}
export default SalespersonForm