import React, { useState, useEffect } from "react";

function SalesPeopleList() {
    const [sales, setSales] = useState([]);
    const [sale, setSale] = useState('');

    const fetchSaleData = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json()
            setSales(data.salespeople);
        }
    };
    
    
    useEffect(() => {
        fetchSaleData();
    }, []);
return (
    <>
    <h1>Salespeople</h1>
    <table className="tabel table-striped">
        <thead>
            <tr>
                <th>Employee ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
        {sales.map((sale) => {
            return(
                <tr key={sale.id} >
                    <td> {sale.employee_id} </td>
                    <td> {sale.first_name} </td>
                    <td> {sale.last_name} </td>
                </tr>
            );
        })}
        </tbody>
    </table>
    </>
);
}
export default SalesPeopleList
