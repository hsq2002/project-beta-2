import {useState, useEffect} from "react";

function AppointmentList() {
    const [appointments, setAppointment] = useState([])
    const loadVehicles = async () => {

        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setAppointment(data.appointments);
        }
    }

    const handleCancel = async (e) =>{
        const url = `http://localhost:8080/api/appointments/${e.target.id}/cancel/`

        const fetchConfig = {
            method: "Put",
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        const data = await response.json();

        setAppointment(
        appointments.filter(
          (appointment) => String(appointment.id) !== e.target.id
          )
        );
    };


    const handleFinish = async (e) => {
        const url = `http://localhost:8080/api/appointments/${e.target.id}/finish/`

        const fetchConfig = {
            method: "Put",
            headers: {
              "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        const data = await response.json();

        setAppointment(
          appointments.filter(
            (appointment) => String(appointment.id) !== e.target.id
          )
        );
    };

    useEffect(() => {
        loadVehicles()
    },[])
    return (
        <>
        <div className="container">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Appointment VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>DateTime</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {appointments?.map(appointment => {
                return (
                  <tr key={appointment.id}>
                    <td>{ appointment.vin }</td>
                    <td> Yes? </td>
                    <td>{ appointment.customer }</td>
                    <td>{ appointment.date_time }</td>
                    <td>
                        { appointment.technician.first_name }{" "}
                        { appointment.technician.last_name }
                    </td>
                    <td>{ appointment.reason }</td>

                    <td>Pending</td>

                    <td>
                        <button onClick={handleCancel}
                        id={appointment.id}
                        className="btn btn-danger">Cancel</button>
                        <button onClick={handleFinish}
                        id={appointment.id}
                        className="btn btn-success">Finish</button>
                    </td>

                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        </>
    )
}

export default AppointmentList;
