import {useState, useEffect} from "react";

function ServiceHistory() {
    const [services, setServices] = useState([])
    const [filterValue, setFilterValue] = useState("");

    const handleFilterValueChange = (e) => {
      const value = e.target.value;
      setFilterValue(value);
    }
    const loadServices = async () => {

        const url = `http://localhost:8080/api/appointments/`;
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setServices(data.appointments);
        }
    }

    useEffect(() => {
        loadServices()
    },[])
    return (
        <>
        <h1>Service History</h1>
        <div className="container">
          <h1>{filterValue}</h1>
          <input value={filterValue} onChange={handleFilterValueChange} placeholder="Search by VIN..." />
          <table className="table table-striped">
            <thead>
              <tr>
                <th>VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>DateTime</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>

              {services
              .filter((appointment) => appointment.vin.includes(filterValue))
              ?.map((appointment) => {
                return (
                    <tr key={appointment.id}>
                    <td>{ appointment.vin }</td>
                    <td>{}</td>
                    <td>{ appointment.customer }</td>
                    <td>{ appointment.date_time }</td>
                    <td>
                        { appointment.technician.first_name }{" "}
                        { appointment.technician.last_name }
                    </td>
                    <td>{ appointment.reason }</td>
                    <td>{ appointment.status }</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        </>
    )
}

export default ServiceHistory
