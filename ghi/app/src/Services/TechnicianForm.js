import React, {useState} from "react";

function TechnicianForm() {
    const [first_name, setFirstname] = useState("");
    const [last_name, setLastname] = useState("");
    const [employee_id, setEmployee] = useState("");

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstname(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastname(value);
    }

    const handlePictureChange = (event) => {
        const value = event.target.value;
        setEmployee(value);
    }



    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;


        const technicianurl = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(technicianurl, fetchConfig);
        if (response.ok) {
            const newtech = await response.json();

            setFirstname(" ");
            setLastname(" ");
            setEmployee(" ");


        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a Technician</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input value={first_name} onChange={handleFirstNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">First name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={last_name} onChange={handleLastNameChange} placeholder="lastName" required type="text" name="lastname" id="lastname" className="form-control" />
                <label htmlFor="lastname">Last name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={employee_id} onChange={handlePictureChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Employee ID</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}


export default TechnicianForm;
