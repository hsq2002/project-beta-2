import {useState, useEffect} from "react";

function TechnicianList() {
    const [technicians, setTechnician] = useState([])
    const LoadTechnicians = async () => {

        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setTechnician(data.technicians);
        }
    };



    useEffect(() => {
        LoadTechnicians()
    },[])

    const handleDelete = async (event) => {
      const url = `http://localhost:8080/api/technicians/${event.target.id}`;

      const fetchConfigs = {
        method: "Delete",
        headers: {
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(url, fetchConfigs);
      const data = await response.json();

      setTechnician(
        technicians.filter(
          (technician) => String(technician.id) !== event.target.id
        )
      );
    };

    return (
        <>
        <div className="container">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Employee ID</th>

              </tr>
            </thead>
            <tbody>
              {technicians?.map(technician => {
                return (
                  <tr key={technician.id}>
                    <td>{ technician.first_name }</td>
                    <td>{ technician.last_name }</td>
                    <td>{ technician.employee_id }</td>
                    <td>
                      <button
                        onClick={handleDelete}
                        id={technician.id}
                        className="btn btn-danger">Delete
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
        </>
    )
}

export default TechnicianList
