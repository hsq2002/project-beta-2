from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.PositiveSmallIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"pk": self.id})


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=200, default="Created")
    customer = models.CharField(max_length=200)
    vin = models.CharField(max_length=17, unique=True)

    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )
    def cancelled(self):
        self.status = "cancelled"
        self.save()

    def finished(self):
        self.status = "finished"
        self.save()


    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.id})
