from django.urls import path

from .views import api_list_technician, api_show_technician, api_list_appointment, api_show_appointment, api_show_appointment_cancel, api_show_appointment_finish

urlpatterns = [
    path("technicians/", api_list_technician, name="api_list_technician"),
    path("technicians/<int:pk>/", api_show_technician, name="api_show_technician"),
    path("appointments/", api_list_appointment, name="api_list_appointment"),
    path("appointments/<int:pk>/", api_show_appointment, name="api_show_appointment"),
    path("appointments/<int:pk>/cancel/", api_show_appointment_cancel, name="api_show_appointment_cancel"),
    path("appointments/<int:pk>/finish/", api_show_appointment_finish, name="api_show_appointment_finish"),
]
