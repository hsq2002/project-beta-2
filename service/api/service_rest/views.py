from django.shortcuts import render
import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment
from django.views.decorators.http import require_http_methods

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id"
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "customer",
        "vin",
        "technician",
        "id"
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            services = Technician.objects.create(**content)
            return JsonResponse(
                services,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Exception as e:
            return JsonResponse(str(e), safe=False)


@require_http_methods(["GET", "DELETE"])
def api_show_technician(request, pk):
    if request.method == "GET":
        try:
            technicians = Technician.objects.get(id=pk)
            return JsonResponse(
                technicians,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": f"technician {pk} does not exist"}
            )
    else:
        try:
            count, _ = Technician.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Technician.DoesNotExist:
            return JsonResponse({"message": f"technician {pk} does not exist"})


@require_http_methods(["GET", "POST"])
def api_list_appointment(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technicians = Technician.objects.get(id=content["technician"])
            content["technician"] = technicians

            appointments = Appointment.objects.create(**content)
            return JsonResponse(
                appointments,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Exception as e:
            return JsonResponse(str(e), safe=False)

@require_http_methods(["GET", "DELETE"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointments,
                encoder=AppointmentEncoder,
                safe=False
            )
        except BaseException:
            return JsonResponse(
                {"message": f"service {pk} does not exist"}
            )
    else:
        try:
            appointments = Appointment.objects.get(id=pk)
            appointments.delete()
            return JsonResponse(
                appointments,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["PUT"])
def api_show_appointment_cancel(request, pk):
    appointments = Appointment.objects.get(id=pk)
    appointments.cancelled()
    return JsonResponse(
        appointments,
        encoder=AppointmentEncoder,
        safe=False
    )


@require_http_methods(["PUT"])
def api_show_appointment_finish(request, pk):
    appointments = Appointment.objects.get(id=pk)
    appointments.finished()
    return JsonResponse(
        appointments,
        encoder=AppointmentEncoder,
        safe=False
    )
